(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/components/app/app-routing.module.ts":
/*!******************************************************!*\
  !*** ./src/app/components/app/app-routing.module.ts ***!
  \******************************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/components/app/app.component.css":
/*!**************************************************!*\
  !*** ./src/app/components/app/app.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div.top{\n    padding-top: 30px;\n    text-align: center;\n}\n\ndiv.top h1, h2{\n    font-weight: bolder;\n}\n\ndiv.top h1{\n    font-size: 1.5em;\n    color: mediumslateblue;\n}\n\ndiv.top h2{\n    font-size: 1.2em;\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxpQkFBaUI7SUFDakIsa0JBQWtCO0FBQ3RCOztBQUVBO0lBQ0ksbUJBQW1CO0FBQ3ZCOztBQUVBO0lBQ0ksZ0JBQWdCO0lBQ2hCLHNCQUFzQjtBQUMxQjs7QUFFQTtJQUNJLGdCQUFnQjtBQUNwQiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZGl2LnRvcHtcbiAgICBwYWRkaW5nLXRvcDogMzBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmRpdi50b3AgaDEsIGgye1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkZXI7XG59XG5cbmRpdi50b3AgaDF7XG4gICAgZm9udC1zaXplOiAxLjVlbTtcbiAgICBjb2xvcjogbWVkaXVtc2xhdGVibHVlO1xufVxuXG5kaXYudG9wIGgye1xuICAgIGZvbnQtc2l6ZTogMS4yZW07XG59XG4iXX0= */"

/***/ }),

/***/ "./src/app/components/app/app.component.html":
/*!***************************************************!*\
  !*** ./src/app/components/app/app.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div class='top'>\n    <h1>Geographical areas</h1>\n    <p>editable tree</p>\n    <p>Angular 7</p>\n</div>\n<div>\n    <app-tree></app-tree>\n</div>\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/components/app/app.component.ts":
/*!*************************************************!*\
  !*** ./src/app/components/app/app.component.ts ***!
  \*************************************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/components/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/components/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/components/app/app.module.ts":
/*!**********************************************!*\
  !*** ./src/app/components/app/app.module.ts ***!
  \**********************************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/components/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/components/app/app.component.ts");
/* harmony import */ var _tree_tree_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../tree/tree.module */ "./src/app/components/tree/tree.module.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _service_service_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../service/service.module */ "./src/app/service/service.module.ts");
/* harmony import */ var _service_TreeData_validate__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../service/TreeData.validate */ "./src/app/service/TreeData.validate.ts");
/* harmony import */ var _service_ChildLengthValidator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../service/ChildLengthValidator */ "./src/app/service/ChildLengthValidator.ts");











var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _tree_tree_module__WEBPACK_IMPORTED_MODULE_5__["TreeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_6__["MatIconModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_7__["NoopAnimationsModule"],
                _service_service_module__WEBPACK_IMPORTED_MODULE_8__["ServiceModule"]
            ],
            providers: [{
                    provide: _service_TreeData_validate__WEBPACK_IMPORTED_MODULE_9__["Validator"],
                    useClass: _service_ChildLengthValidator__WEBPACK_IMPORTED_MODULE_10__["ChildLengthValidator"]
                }],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/tree/button/button.component.css":
/*!*************************************************************!*\
  !*** ./src/app/components/tree/button/button.component.css ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdHJlZS9idXR0b24vYnV0dG9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/tree/button/button.component.html":
/*!**************************************************************!*\
  !*** ./src/app/components/tree/button/button.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button mat-icon-button (click)=\"openDialog()\">\n    <mat-icon color=primary\n              class=\"mat-icon-rtl-mirror material-icons-outlined\">\n        <ng-content></ng-content>\n    </mat-icon>\n</button>\n"

/***/ }),

/***/ "./src/app/components/tree/button/button.component.ts":
/*!************************************************************!*\
  !*** ./src/app/components/tree/button/button.component.ts ***!
  \************************************************************/
/*! exports provided: ButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ButtonComponent", function() { return ButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _popup_form_add_add_popup_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../popup-form/add/add-popup.component */ "./src/app/components/tree/popup-form/add/add-popup.component.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _type_CountryEnum__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../type/CountryEnum */ "./src/app/type/CountryEnum.ts");
/* harmony import */ var _service_redux_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../service/redux/store */ "./src/app/service/redux/store.ts");
/* harmony import */ var _popup_form_edit_edit_popup_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../popup-form/edit/edit-popup.component */ "./src/app/components/tree/popup-form/edit/edit-popup.component.ts");







var ButtonComponent = /** @class */ (function () {
    function ButtonComponent(dialog) {
        this.dialog = dialog;
    }
    Object.defineProperty(ButtonComponent.prototype, "node", {
        set: function (node) {
            this._node = node;
        },
        enumerable: true,
        configurable: true
    });
    ButtonComponent.prototype.ngOnInit = function () {
    };
    ButtonComponent.prototype.openDialog = function () {
        var data = {
            data: {
                node: this._node
            }
        };
        switch (this.updateType) {
            case _service_redux_store__WEBPACK_IMPORTED_MODULE_5__["UpdateType"].add:
                this.dialog.open(_popup_form_add_add_popup_component__WEBPACK_IMPORTED_MODULE_2__["AddPopupDialogComponent"], data);
                break;
            case _service_redux_store__WEBPACK_IMPORTED_MODULE_5__["UpdateType"].edit:
                this.dialog.open(_popup_form_edit_edit_popup_component__WEBPACK_IMPORTED_MODULE_6__["EditPopupDialogComponent"], data);
                break;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ButtonComponent.prototype, "updateType", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], ButtonComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], ButtonComponent.prototype, "node", null);
    ButtonComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-button',
            template: __webpack_require__(/*! ./button.component.html */ "./src/app/components/tree/button/button.component.html"),
            styles: [__webpack_require__(/*! ./button.component.css */ "./src/app/components/tree/button/button.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"]])
    ], ButtonComponent);
    return ButtonComponent;
}());



/***/ }),

/***/ "./src/app/components/tree/deleteButton/deleteButton.component.css":
/*!*************************************************************************!*\
  !*** ./src/app/components/tree/deleteButton/deleteButton.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdHJlZS9kZWxldGVCdXR0b24vZGVsZXRlQnV0dG9uLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./src/app/components/tree/deleteButton/deleteButton.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/components/tree/deleteButton/deleteButton.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<button mat-icon-button (click)=\"saveChanges()\">\n    <mat-icon color=primary\n              class=\"mat-icon-rtl-mirror material-icons-outlined\">\n        delete\n    </mat-icon>\n</button>\n"

/***/ }),

/***/ "./src/app/components/tree/deleteButton/deleteButton.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/components/tree/deleteButton/deleteButton.component.ts ***!
  \************************************************************************/
/*! exports provided: DeleteButtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeleteButtonComponent", function() { return DeleteButtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _service_TreeData_io_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../service/TreeData.io.service */ "./src/app/service/TreeData.io.service.ts");
/* harmony import */ var _service_redux_store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../service/redux/store */ "./src/app/service/redux/store.ts");
/* harmony import */ var _service_TreeData_modify__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../service/TreeData.modify */ "./src/app/service/TreeData.modify.ts");





var DeleteButtonComponent = /** @class */ (function () {
    function DeleteButtonComponent(service) {
        this.service = service;
    }
    DeleteButtonComponent.prototype.ngOnInit = function () {
    };
    DeleteButtonComponent.prototype.saveChanges = function () {
        saveChanges(this.uuid, this.service);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], DeleteButtonComponent.prototype, "uuid", void 0);
    DeleteButtonComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-button-delete',
            template: __webpack_require__(/*! ./deleteButton.component.html */ "./src/app/components/tree/deleteButton/deleteButton.component.html"),
            styles: [__webpack_require__(/*! ./deleteButton.component.css */ "./src/app/components/tree/deleteButton/deleteButton.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_TreeData_io_service__WEBPACK_IMPORTED_MODULE_2__["TreeDataIoService"]])
    ], DeleteButtonComponent);
    return DeleteButtonComponent;
}());

function saveChanges(id, service) {
    service.getTree().subscribe(function (tree) {
        Object(_service_TreeData_modify__WEBPACK_IMPORTED_MODULE_4__["deleteNode"])(id, tree);
        service.putTree(tree);
        _service_redux_store__WEBPACK_IMPORTED_MODULE_3__["reduxStore"].dispatch({ type: _service_redux_store__WEBPACK_IMPORTED_MODULE_3__["UpdateType"].delete });
    });
}


/***/ }),

/***/ "./src/app/components/tree/popup-form/add/add-popup.component.html":
/*!*************************************************************************!*\
  !*** ./src/app/components/tree/popup-form/add/add-popup.component.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>Add new {{childType}}\n    to {{node.name}}\n</p>\n\n<p>\n    <mat-form-field class=\"example-full-width\">\n    <!--suppress TypeScriptUnresolvedVariable -->\n        <input matInput [placeholder]=\"childType\" (input)=\"name=$event.target.value\" [value]=\"name\">\n    </mat-form-field>\n</p>\n\n<div mat-dialog-actions>\n    <button mat-button (click)=\"onNoClick()\">Cancel</button>\n    <button mat-button [mat-dialog-close]=\"'save'\" cdkFocusInitial>Ok</button>\n</div>"

/***/ }),

/***/ "./src/app/components/tree/popup-form/add/add-popup.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/components/tree/popup-form/add/add-popup.component.ts ***!
  \***********************************************************************/
/*! exports provided: AddPopupDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddPopupDialogComponent", function() { return AddPopupDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _service_TreeData_io_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../service/TreeData.io.service */ "./src/app/service/TreeData.io.service.ts");
/* harmony import */ var _service_TreeData_uuid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../service/TreeData.uuid */ "./src/app/service/TreeData.uuid.ts");
/* harmony import */ var _type_CountryEnum__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../type/CountryEnum */ "./src/app/type/CountryEnum.ts");
/* harmony import */ var _service_redux_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../../service/redux/store */ "./src/app/service/redux/store.ts");







var AddPopupDialogComponent = /** @class */ (function () {
    function AddPopupDialogComponent(dialogRef, service, data) {
        this.dialogRef = dialogRef;
        this.service = service;
        this.data = data;
    }
    Object.defineProperty(AddPopupDialogComponent.prototype, "node", {
        get: function () {
            return this.data.node;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AddPopupDialogComponent.prototype, "childType", {
        get: function () {
            return Object(_type_CountryEnum__WEBPACK_IMPORTED_MODULE_5__["childType"])(this.node.type);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AddPopupDialogComponent.prototype, "name", {
        get: function () {
            return this._name || '';
        },
        set: function (val) {
            this._name = val;
        },
        enumerable: true,
        configurable: true
    });
    //  events
    AddPopupDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    AddPopupDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dialogRef.afterClosed().subscribe(function (result) {
            if (result === 'save') {
                saveChanges(_this.node.uuid, _this.childType, _this.name, _this.service);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AddPopupDialogComponent.prototype, "node", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], AddPopupDialogComponent.prototype, "childType", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [String])
    ], AddPopupDialogComponent.prototype, "name", null);
    AddPopupDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'add-popup-dialog',
            template: __webpack_require__(/*! ./add-popup.component.html */ "./src/app/components/tree/popup-form/add/add-popup.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _service_TreeData_io_service__WEBPACK_IMPORTED_MODULE_3__["TreeDataIoService"], Object])
    ], AddPopupDialogComponent);
    return AddPopupDialogComponent;
}());

function saveChanges(parent, type, name, service) {
    service.getTree().subscribe(function (tree) {
        var uuid = Object(_service_TreeData_uuid__WEBPACK_IMPORTED_MODULE_4__["genUuid"])(), newNode = {
            name: name,
            uuid: uuid,
            parent: parent,
            type: type
        };
        tree.push(newNode);
        service.putTree(tree);
        _service_redux_store__WEBPACK_IMPORTED_MODULE_6__["reduxStore"].dispatch({ type: _service_redux_store__WEBPACK_IMPORTED_MODULE_6__["UpdateType"].add });
    });
}


/***/ }),

/***/ "./src/app/components/tree/popup-form/edit/edit-popup.component.html":
/*!***************************************************************************!*\
  !*** ./src/app/components/tree/popup-form/edit/edit-popup.component.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>Rename {{node.name}}</p>\n\n<p>\n    <mat-form-field class=\"example-full-width\">\n        <!--suppress TypeScriptUnresolvedVariable -->\n        <input matInput [placeholder]=\"node.type\" (input)=\"name=$event.target.value\" [value]=\"name\">\n    </mat-form-field>\n</p>\n\n<div mat-dialog-actions>\n    <button mat-button (click)=\"onNoClick()\">Cancel</button>\n    <button mat-button [mat-dialog-close]=\"'save'\" cdkFocusInitial>Ok</button>\n</div>"

/***/ }),

/***/ "./src/app/components/tree/popup-form/edit/edit-popup.component.ts":
/*!*************************************************************************!*\
  !*** ./src/app/components/tree/popup-form/edit/edit-popup.component.ts ***!
  \*************************************************************************/
/*! exports provided: EditPopupDialogComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditPopupDialogComponent", function() { return EditPopupDialogComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _service_TreeData_io_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../service/TreeData.io.service */ "./src/app/service/TreeData.io.service.ts");
/* harmony import */ var _service_TreeData_uuid__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../../service/TreeData.uuid */ "./src/app/service/TreeData.uuid.ts");
/* harmony import */ var _service_redux_store__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../../service/redux/store */ "./src/app/service/redux/store.ts");






var EditPopupDialogComponent = /** @class */ (function () {
    function EditPopupDialogComponent(dialogRef, service, data) {
        this.dialogRef = dialogRef;
        this.service = service;
        this.data = data;
    }
    Object.defineProperty(EditPopupDialogComponent.prototype, "node", {
        get: function () {
            return this.data.node;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(EditPopupDialogComponent.prototype, "name", {
        get: function () {
            return this._name || this.data.node.name;
        },
        set: function (val) {
            this._name = val;
        },
        enumerable: true,
        configurable: true
    });
    //  events
    EditPopupDialogComponent.prototype.onNoClick = function () {
        this.dialogRef.close();
    };
    EditPopupDialogComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dialogRef.afterClosed().subscribe(function (result) {
            if (result === 'save') {
                saveChanges(_this.node, _this.name, _this.service);
            }
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], EditPopupDialogComponent.prototype, "node", null);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [String])
    ], EditPopupDialogComponent.prototype, "name", null);
    EditPopupDialogComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'edit-popup-dialog',
            template: __webpack_require__(/*! ./edit-popup.component.html */ "./src/app/components/tree/popup-form/edit/edit-popup.component.html"),
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](2, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_DIALOG_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogRef"],
            _service_TreeData_io_service__WEBPACK_IMPORTED_MODULE_3__["TreeDataIoService"], Object])
    ], EditPopupDialogComponent);
    return EditPopupDialogComponent;
}());

function saveChanges(node, name, service) {
    service.getTree().subscribe(function (tree) {
        var node1 = Object(_service_TreeData_uuid__WEBPACK_IMPORTED_MODULE_4__["findNodeByUuid"])(node.uuid, tree);
        node1.name = name;
        service.putTree(tree);
        _service_redux_store__WEBPACK_IMPORTED_MODULE_5__["reduxStore"].dispatch({ type: _service_redux_store__WEBPACK_IMPORTED_MODULE_5__["UpdateType"].edit });
    });
}


/***/ }),

/***/ "./src/app/components/tree/tree.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/tree/tree.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-tree-invisible {\n    display: none;\n}\n\n.example-tree ul,\n.example-tree li {\n    margin-top: 0;\n    margin-bottom: 0;\n    list-style-type: none;\n}\n\ndiv.treeWrap, div.buttonWrap {\n    display: inline-block;\n    margin-top: 30px;\n    margin-left: 10%;\n}\n\ndiv.buttonWrap {\n    float:right;\n    margin-right: 200px;\n}\n\ndiv.nodeName {\n    display: inline-block;\n    min-width: 110px;\n    margin-right: 50px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90cmVlL3RyZWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7QUFDakI7O0FBRUE7O0lBRUksYUFBYTtJQUNiLGdCQUFnQjtJQUNoQixxQkFBcUI7QUFDekI7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGdCQUFnQjtBQUNwQjs7QUFFQTtJQUNJLFdBQVc7SUFDWCxtQkFBbUI7QUFDdkI7O0FBRUE7SUFDSSxxQkFBcUI7SUFDckIsZ0JBQWdCO0lBQ2hCLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvdHJlZS90cmVlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS10cmVlLWludmlzaWJsZSB7XG4gICAgZGlzcGxheTogbm9uZTtcbn1cblxuLmV4YW1wbGUtdHJlZSB1bCxcbi5leGFtcGxlLXRyZWUgbGkge1xuICAgIG1hcmdpbi10b3A6IDA7XG4gICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICBsaXN0LXN0eWxlLXR5cGU6IG5vbmU7XG59XG5cbmRpdi50cmVlV3JhcCwgZGl2LmJ1dHRvbldyYXAge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtYXJnaW4tdG9wOiAzMHB4O1xuICAgIG1hcmdpbi1sZWZ0OiAxMCU7XG59XG5cbmRpdi5idXR0b25XcmFwIHtcbiAgICBmbG9hdDpyaWdodDtcbiAgICBtYXJnaW4tcmlnaHQ6IDIwMHB4O1xufVxuXG5kaXYubm9kZU5hbWUge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICBtaW4td2lkdGg6IDExMHB4O1xuICAgIG1hcmdpbi1yaWdodDogNTBweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/tree/tree.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/tree/tree.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"display\" class='treeWrap'>\n    <mat-tree\n            [dataSource]=\"dataSource\"\n            [treeControl]=\"treeControl\"\n            [trackBy]=\"trackBy\"\n            class=\"example-tree\">\n\n        <!-- This is the tree node template for leaf nodes -->\n        <mat-tree-node *matTreeNodeDef=\"let node\" matTreeNodeToggle>\n            <li class=\"mat-tree-node\">\n                <!-- use a disabled button to provide padding for tree leaf -->\n                <button mat-icon-button disabled></button>\n\n                <div class='nodeName'>{{node.name}}</div>\n\n                <app-button [updateType]=\"editType\" [node]=\"node\">edit</app-button>\n                <app-button-delete [uuid]=\"node.uuid\"></app-button-delete>\n                <div *ngIf=\"node.type != city\">\n                    <app-button [updateType]=\"addType\" [node]=\"node\">add_box</app-button>\n                </div>\n            </li>\n        </mat-tree-node>\n        <!-- This is the tree node template for expandable nodes -->\n        <mat-nested-tree-node *matTreeNodeDef=\"let node; when: hasChildBound\">\n            <li>\n                <div class=\"mat-tree-node\">\n                    <button mat-icon-button matTreeNodeToggle\n                            [attr.aria-label]=\"'toggle ' + node.name\">\n                        <mat-icon class=\"mat-icon-rtl-mirror\">\n                            {{treeControl.isExpanded(node) ? 'expand_more' : 'chevron_right'}}\n                        </mat-icon>\n                    </button>\n                    \n                    <div class='nodeName'>{{node.name}}</div>\n\n                    <app-button [updateType]=\"editType\" [node]=\"node\">edit</app-button>\n                    <app-button-delete [uuid]=\"node.uuid\"></app-button-delete>\n                    <app-button [updateType]=\"addType\" [node]=\"node\">add_box</app-button>\n                </div>\n                <ul [class.example-tree-invisible]=\"!treeControl.isExpanded(node)\">\n                    <ng-container matTreeNodeOutlet></ng-container>\n                </ul>\n            </li>\n        </mat-nested-tree-node>\n    </mat-tree>\n</div>\n\n<div class='buttonWrap'>\n    <button mat-button (click)=\"validate()\">Validate</button>        \n</div>"

/***/ }),

/***/ "./src/app/components/tree/tree.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/tree/tree.component.ts ***!
  \***************************************************/
/*! exports provided: TreeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TreeComponent", function() { return TreeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/cdk/tree */ "./node_modules/@angular/cdk/esm5/tree.es5.js");
/* harmony import */ var _service_TreeData_io_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/TreeData.io.service */ "./src/app/service/TreeData.io.service.ts");
/* harmony import */ var _type_CountryEnum__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../type/CountryEnum */ "./src/app/type/CountryEnum.ts");
/* harmony import */ var _service_redux_store__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../service/redux/store */ "./src/app/service/redux/store.ts");
/* harmony import */ var _service_TreeData_getChildren__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../service/TreeData.getChildren */ "./src/app/service/TreeData.getChildren.ts");
/* harmony import */ var _service_TreeData_validate__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../service/TreeData.validate */ "./src/app/service/TreeData.validate.ts");
/* harmony import */ var _validation_summary_validation_summary_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./validation-summary/validation-summary.component */ "./src/app/components/tree/validation-summary/validation-summary.component.ts");










var TreeComponent = /** @class */ (function () {
    function TreeComponent(ioService, bottomSheet, validator) {
        this.ioService = ioService;
        this.bottomSheet = bottomSheet;
        this.validator = validator;
        this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTreeNestedDataSource"]();
        this.treeControl = new _angular_cdk_tree__WEBPACK_IMPORTED_MODULE_3__["NestedTreeControl"](this.getChildren.bind(this));
        //  used to refresh tree
        this.display = true;
        this.city = _type_CountryEnum__WEBPACK_IMPORTED_MODULE_5__["CountryEnum"].City;
        this.unsubscribe = function () { return false; };
        this.addType = _service_redux_store__WEBPACK_IMPORTED_MODULE_6__["UpdateType"].add;
        this.editType = _service_redux_store__WEBPACK_IMPORTED_MODULE_6__["UpdateType"].edit;
        this.hasChildBound = this.hasChild.bind(this);
    }
    TreeComponent.prototype.getChildren = function (node) {
        return Object(_service_TreeData_getChildren__WEBPACK_IMPORTED_MODULE_7__["getChildren"])(this.fullListData, node);
    };
    TreeComponent.prototype.pullData = function () {
        var _this = this;
        this.ioService.getTree().subscribe(function (data) {
            _this.fullListData = data;
            _this.dataSource.data = data.filter(function (n) { return !n.parent; });
        });
    };
    TreeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.pullData();
        this.unsubscribe = _service_redux_store__WEBPACK_IMPORTED_MODULE_6__["reduxStore"].subscribe(function () {
            var data = _this.dataSource.data;
            _this.dataSource.data = null;
            _this.dataSource.data = data;
        });
    };
    TreeComponent.prototype.hasChild = function (_, node) {
        return this.getChildren(node).length > 0;
    };
    TreeComponent.prototype.trackBy = function (idx, item) {
        return item.uuid;
    };
    TreeComponent.prototype.validate = function () {
        var result = Object(_service_TreeData_validate__WEBPACK_IMPORTED_MODULE_8__["validateList"])(this.fullListData, this.validator);
        if (result instanceof Array) {
            var resultSorted = result.sort(function (a, b) { return a[2].name > b[2].name ? 1 : -1; });
            this.bottomSheet.open(_validation_summary_validation_summary_component__WEBPACK_IMPORTED_MODULE_9__["ValidationSummaryComponent"], { data: resultSorted });
        }
    };
    TreeComponent.prototype.ngOnDestroy = function () {
        this.unsubscribe();
    };
    TreeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tree',
            template: __webpack_require__(/*! ./tree.component.html */ "./src/app/components/tree/tree.component.html"),
            styles: [__webpack_require__(/*! ./tree.component.css */ "./src/app/components/tree/tree.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_service_TreeData_io_service__WEBPACK_IMPORTED_MODULE_4__["TreeDataIoService"],
            _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBottomSheet"],
            _service_TreeData_validate__WEBPACK_IMPORTED_MODULE_8__["Validator"]])
    ], TreeComponent);
    return TreeComponent;
}());



/***/ }),

/***/ "./src/app/components/tree/tree.module.ts":
/*!************************************************!*\
  !*** ./src/app/components/tree/tree.module.ts ***!
  \************************************************/
/*! exports provided: TreeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TreeModule", function() { return TreeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material_tree__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material/tree */ "./node_modules/@angular/material/esm5/tree.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/cdk/overlay */ "./node_modules/@angular/cdk/esm5/overlay.es5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _tree_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./tree.component */ "./src/app/components/tree/tree.component.ts");
/* harmony import */ var _service_service_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../service/service.module */ "./src/app/service/service.module.ts");
/* harmony import */ var _button_button_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./button/button.component */ "./src/app/components/tree/button/button.component.ts");
/* harmony import */ var _popup_form_add_add_popup_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./popup-form/add/add-popup.component */ "./src/app/components/tree/popup-form/add/add-popup.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _popup_form_edit_edit_popup_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./popup-form/edit/edit-popup.component */ "./src/app/components/tree/popup-form/edit/edit-popup.component.ts");
/* harmony import */ var _validation_summary_validation_summary_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./validation-summary/validation-summary.component */ "./src/app/components/tree/validation-summary/validation-summary.component.ts");
/* harmony import */ var _deleteButton_deleteButton_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./deleteButton/deleteButton.component */ "./src/app/components/tree/deleteButton/deleteButton.component.ts");















var TreeModule = /** @class */ (function () {
    function TreeModule() {
    }
    TreeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_tree_component__WEBPACK_IMPORTED_MODULE_6__["TreeComponent"],
                _button_button_component__WEBPACK_IMPORTED_MODULE_8__["ButtonComponent"],
                _deleteButton_deleteButton_component__WEBPACK_IMPORTED_MODULE_13__["DeleteButtonComponent"],
                _popup_form_add_add_popup_component__WEBPACK_IMPORTED_MODULE_9__["AddPopupDialogComponent"],
                _popup_form_edit_edit_popup_component__WEBPACK_IMPORTED_MODULE_11__["EditPopupDialogComponent"],
                _validation_summary_validation_summary_component__WEBPACK_IMPORTED_MODULE_12__["ValidationSummaryComponent"]],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_5__["CommonModule"],
                _angular_material_tree__WEBPACK_IMPORTED_MODULE_2__["MatTreeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatButtonModule"],
                _service_service_module__WEBPACK_IMPORTED_MODULE_7__["ServiceModule"],
                _angular_cdk_overlay__WEBPACK_IMPORTED_MODULE_4__["OverlayModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatBottomSheetModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_10__["FormsModule"]
            ],
            providers: [_angular_material__WEBPACK_IMPORTED_MODULE_3__["MatDialog"], _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatBottomSheet"]],
            exports: [_tree_component__WEBPACK_IMPORTED_MODULE_6__["TreeComponent"]],
            entryComponents: [
                _popup_form_add_add_popup_component__WEBPACK_IMPORTED_MODULE_9__["AddPopupDialogComponent"],
                _popup_form_edit_edit_popup_component__WEBPACK_IMPORTED_MODULE_11__["EditPopupDialogComponent"],
                _validation_summary_validation_summary_component__WEBPACK_IMPORTED_MODULE_12__["ValidationSummaryComponent"],
                _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatBottomSheetContainer"]
            ]
        })
    ], TreeModule);
    return TreeModule;
}());



/***/ }),

/***/ "./src/app/components/tree/validation-summary/validation-summary.component.css":
/*!*************************************************************************************!*\
  !*** ./src/app/components/tree/validation-summary/validation-summary.component.css ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "h1 {\n    font-size: 1.2em;\n    color: tomato;\n}\n\ndiv.validation div {\n    display: inline-block;\n}\n\ndiv.validation div:nth-of-type(1){\n    min-width: 100px;\n}\n\ndiv.validation div:nth-of-type(2){\n    margin-left: 15px;\n    width: 200px;\n    text-align: right;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90cmVlL3ZhbGlkYXRpb24tc3VtbWFyeS92YWxpZGF0aW9uLXN1bW1hcnkuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGdCQUFnQjtJQUNoQixhQUFhO0FBQ2pCOztBQUVBO0lBQ0kscUJBQXFCO0FBQ3pCOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0lBQ0ksaUJBQWlCO0lBQ2pCLFlBQVk7SUFDWixpQkFBaUI7QUFDckIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3RyZWUvdmFsaWRhdGlvbi1zdW1tYXJ5L3ZhbGlkYXRpb24tc3VtbWFyeS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDEge1xuICAgIGZvbnQtc2l6ZTogMS4yZW07XG4gICAgY29sb3I6IHRvbWF0bztcbn1cblxuZGl2LnZhbGlkYXRpb24gZGl2IHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbmRpdi52YWxpZGF0aW9uIGRpdjpudGgtb2YtdHlwZSgxKXtcbiAgICBtaW4td2lkdGg6IDEwMHB4O1xufVxuXG5kaXYudmFsaWRhdGlvbiBkaXY6bnRoLW9mLXR5cGUoMil7XG4gICAgbWFyZ2luLWxlZnQ6IDE1cHg7XG4gICAgd2lkdGg6IDIwMHB4O1xuICAgIHRleHQtYWxpZ246IHJpZ2h0O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/tree/validation-summary/validation-summary.component.html":
/*!**************************************************************************************!*\
  !*** ./src/app/components/tree/validation-summary/validation-summary.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<h1>Validation Summary</h1>\n\n<div *ngFor=\"let msg of validateResult\" class='validation'>\n    <div>{{msg[2].name}}</div> \n    <div>{{msg[1]}}</div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/tree/validation-summary/validation-summary.component.ts":
/*!************************************************************************************!*\
  !*** ./src/app/components/tree/validation-summary/validation-summary.component.ts ***!
  \************************************************************************************/
/*! exports provided: ValidationSummaryComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ValidationSummaryComponent", function() { return ValidationSummaryComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");




var ValidationSummaryComponent = /** @class */ (function () {
    function ValidationSummaryComponent(validateResult, bottomSheetRef) {
        this.validateResult = validateResult;
        this.bottomSheetRef = bottomSheetRef;
    }
    ValidationSummaryComponent.prototype.ngOnInit = function () {
    };
    ValidationSummaryComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-validation-summary',
            template: __webpack_require__(/*! ./validation-summary.component.html */ "./src/app/components/tree/validation-summary/validation-summary.component.html"),
            styles: [__webpack_require__(/*! ./validation-summary.component.css */ "./src/app/components/tree/validation-summary/validation-summary.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_material__WEBPACK_IMPORTED_MODULE_2__["MAT_BOTTOM_SHEET_DATA"])),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object, _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBottomSheetRef"]])
    ], ValidationSummaryComponent);
    return ValidationSummaryComponent;
}());



/***/ }),

/***/ "./src/app/service/ChildLengthValidator.ts":
/*!*************************************************!*\
  !*** ./src/app/service/ChildLengthValidator.ts ***!
  \*************************************************/
/*! exports provided: ChildLengthValidator, validateChildLength */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChildLengthValidator", function() { return ChildLengthValidator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateChildLength", function() { return validateChildLength; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _TreeData_getChildren__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TreeData.getChildren */ "./src/app/service/TreeData.getChildren.ts");
/* harmony import */ var _type_CountryEnum__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../type/CountryEnum */ "./src/app/type/CountryEnum.ts");
/* harmony import */ var _TreeData_validate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./TreeData.validate */ "./src/app/service/TreeData.validate.ts");

var _a, _b;



var ChildrenLength = (_a = {},
    _a[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_2__["CountryEnum"].Country] = [3, 9],
    _a[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_2__["CountryEnum"].State] = [2, 8],
    _a[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_2__["CountryEnum"].Region] = [1, 7],
    _a[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_2__["CountryEnum"].City] = [0, 0],
    _a);
var _children = (_b = {},
    _b[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_2__["CountryEnum"].Country] = 'states',
    _b[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_2__["CountryEnum"].State] = 'regions',
    _b[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_2__["CountryEnum"].Region] = 'cities',
    _b);
function children(type) {
    return _children[type] || '';
}
function validateChildLength(length, min, max, node) {
    if (length >= min && length <= max)
        return 'ok';
    return [['error', "expected " + min + "..." + max + " " + children(node.type) + ", got: " + length, node]];
}
var ChildLengthValidator = /** @class */ (function (_super) {
    tslib__WEBPACK_IMPORTED_MODULE_0__["__extends"](ChildLengthValidator, _super);
    function ChildLengthValidator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ChildLengthValidator.prototype.validate = function (tree, node) {
        var children = Object(_TreeData_getChildren__WEBPACK_IMPORTED_MODULE_1__["getChildren"])(tree, node), length = children.length, minMax = ChildrenLength[node.type];
        return validateChildLength(length, minMax[0], minMax[1], node);
    };
    return ChildLengthValidator;
}(_TreeData_validate__WEBPACK_IMPORTED_MODULE_3__["Validator"]));



/***/ }),

/***/ "./src/app/service/TreeData.getChildren.ts":
/*!*************************************************!*\
  !*** ./src/app/service/TreeData.getChildren.ts ***!
  \*************************************************/
/*! exports provided: getChildren */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getChildren", function() { return getChildren; });
function getChildren(tree, node) {
    return tree ? tree.filter(function (n) { return n.parent === node.uuid; }) : [];
}



/***/ }),

/***/ "./src/app/service/TreeData.io.service.ts":
/*!************************************************!*\
  !*** ./src/app/service/TreeData.io.service.ts ***!
  \************************************************/
/*! exports provided: TreeDataIoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TreeDataIoService", function() { return TreeDataIoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _TreeData_uuid__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./TreeData.uuid */ "./src/app/service/TreeData.uuid.ts");
/* harmony import */ var rxjs_internal_operators_tap__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/internal/operators/tap */ "./node_modules/rxjs/internal/operators/tap.js");
/* harmony import */ var rxjs_internal_operators_tap__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators_tap__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _observable__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./observable */ "./src/app/service/observable.ts");








var dataKey = 'treeData';
var TreeDataIoService = /** @class */ (function () {
    function TreeDataIoService(http) {
        this.http = http;
        this.baseUrl = 'assets/';
    }
    TreeDataIoService_1 = TreeDataIoService;
    TreeDataIoService.prototype.getTree = function () {
        var _this = this;
        if (!this._cache) {
            var localStored = TreeDataIoService_1.getFromLocalStorage();
            if (localStored) {
                this._cache = localStored;
            }
        }
        if (this._cache) {
            return Object(_observable__WEBPACK_IMPORTED_MODULE_7__["observableFrom"])(this._cache);
        }
        return this.http.get(this.baseUrl + 'treeData.json')
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(TreeDataIoService_1.handleError), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(_TreeData_uuid__WEBPACK_IMPORTED_MODULE_5__["fillinUuid"]), Object(rxjs_internal_operators_tap__WEBPACK_IMPORTED_MODULE_6__["tap"])(function (tree) { return _this.putTree(tree); }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["map"])(function () { return _this._cache; }));
    };
    TreeDataIoService.prototype.putTree = function (data) {
        this._cache = data;
        TreeDataIoService_1.writeToLocalStorage(data);
    };
    TreeDataIoService.getFromLocalStorage = function () {
        var stringData = localStorage.getItem(dataKey);
        if (!stringData)
            return null;
        return JSON.parse(stringData);
    };
    TreeDataIoService.writeToLocalStorage = function (data) {
        var stringData = JSON.stringify(data);
        localStorage.setItem(dataKey, stringData);
    };
    TreeDataIoService.handleError = function (error) {
        console.error('server error:', error);
        if (error.error instanceof Error) {
            var errMessage = error.error.message;
            return rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"].throw(errMessage);
            // Use the following instead if using lite-server
            // return Observable.throw(err.text() || 'backend server error');
        }
        return rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"].throw(error || 'Node.js server error');
    };
    var TreeDataIoService_1;
    TreeDataIoService = TreeDataIoService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], TreeDataIoService);
    return TreeDataIoService;
}());



/***/ }),

/***/ "./src/app/service/TreeData.modify.ts":
/*!********************************************!*\
  !*** ./src/app/service/TreeData.modify.ts ***!
  \********************************************/
/*! exports provided: addNode, editNode, deleteNode */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addNode", function() { return addNode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "editNode", function() { return editNode; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "deleteNode", function() { return deleteNode; });
/* harmony import */ var _type_CountryEnum__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../type/CountryEnum */ "./src/app/type/CountryEnum.ts");
/* harmony import */ var _TreeData_uuid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./TreeData.uuid */ "./src/app/service/TreeData.uuid.ts");
/* harmony import */ var _TreeData_getChildren__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./TreeData.getChildren */ "./src/app/service/TreeData.getChildren.ts");
var _a;



var childTypes = (_a = {},
    _a[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_0__["CountryEnum"].Country] = _type_CountryEnum__WEBPACK_IMPORTED_MODULE_0__["CountryEnum"].State,
    _a[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_0__["CountryEnum"].State] = _type_CountryEnum__WEBPACK_IMPORTED_MODULE_0__["CountryEnum"].Region,
    _a[_type_CountryEnum__WEBPACK_IMPORTED_MODULE_0__["CountryEnum"].Region] = _type_CountryEnum__WEBPACK_IMPORTED_MODULE_0__["CountryEnum"].City,
    _a);
/** returns tuple [new node Uuid, tree]
 * */
function addNode(name, parentId, tree) {
    var parent = Object(_TreeData_uuid__WEBPACK_IMPORTED_MODULE_1__["findNodeByUuid"])(parentId, tree);
    if (!parent)
        throw Error('parent not found');
    var newNodeType = childTypes[parent.type];
    if (!newNodeType)
        throw Error("child type for " + parent.type + " n/a");
    var newNode = {
        name: name,
        type: newNodeType,
        uuid: Object(_TreeData_uuid__WEBPACK_IMPORTED_MODULE_1__["genUuid"])(),
        parent: parentId
    };
    tree.push(newNode);
    return [newNode.uuid, tree];
}
function editNode(id, name, tree) {
    var node = Object(_TreeData_uuid__WEBPACK_IMPORTED_MODULE_1__["findNodeByUuid"])(id, tree);
    if (!node)
        throw Error('node not found');
    node.name = name;
    return tree;
}
function deleteNode(id, tree) {
    var node = Object(_TreeData_uuid__WEBPACK_IMPORTED_MODULE_1__["findNodeByUuid"])(id, tree), children = Object(_TreeData_getChildren__WEBPACK_IMPORTED_MODULE_2__["getChildren"])(tree, node), idx = tree.indexOf(node);
    for (var idx_1 = children.length - 1; idx_1 > -1; idx_1--) {
        deleteNode(children[idx_1].uuid, tree);
    }
    tree.splice(idx, 1);
    return tree;
}



/***/ }),

/***/ "./src/app/service/TreeData.uuid.ts":
/*!******************************************!*\
  !*** ./src/app/service/TreeData.uuid.ts ***!
  \******************************************/
/*! exports provided: genUuid, fillinUuidFolder, fillinUuid, findNodeByUuid */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "genUuid", function() { return genUuid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fillinUuidFolder", function() { return fillinUuidFolder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fillinUuid", function() { return fillinUuid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "findNodeByUuid", function() { return findNodeByUuid; });
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! uuid/v4 */ "./node_modules/uuid/v4.js");
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(uuid_v4__WEBPACK_IMPORTED_MODULE_0__);

function genUuid() {
    return uuid_v4__WEBPACK_IMPORTED_MODULE_0__();
}
var fillinUuidFolder = function (node) {
    if (!node.uuid) {
        node.uuid = genUuid();
    }
    return node;
};
function fillinUuid(tree) {
    return tree.map(fillinUuidFolder);
}
function findNodeByUuid(id, tree) {
    return tree.find(function (n) { return n.uuid === id; }) || null;
}



/***/ }),

/***/ "./src/app/service/TreeData.validate.ts":
/*!**********************************************!*\
  !*** ./src/app/service/TreeData.validate.ts ***!
  \**********************************************/
/*! exports provided: validateList, reduceResults, Validator */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateList", function() { return validateList; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reduceResults", function() { return reduceResults; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Validator", function() { return Validator; });
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/* harmony import */ var lodash__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(lodash__WEBPACK_IMPORTED_MODULE_0__);

function reduceResults(one, acc) {
    if (one === 'ok') {
        return acc;
    }
    else {
        if (acc === 'ok') {
            return one;
        }
        else {
            return one.concat(acc);
        }
    }
}
function validateList(list, validator) {
    return Object(lodash__WEBPACK_IMPORTED_MODULE_0__["reduce"])(list, function (acc, t) {
        var one = validator.validate(list, t);
        return reduceResults(one, acc);
    }, 'ok');
}
var Validator = /** @class */ (function () {
    function Validator() {
    }
    return Validator;
}());



/***/ }),

/***/ "./src/app/service/observable.ts":
/*!***************************************!*\
  !*** ./src/app/service/observable.ts ***!
  \***************************************/
/*! exports provided: observableFrom, observableFromArray */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "observableFrom", function() { return observableFrom; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "observableFromArray", function() { return observableFromArray; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");


function observableFromArray(data) {
    function iter() {
        var _a, _b, _i, i;
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_c) {
            switch (_c.label) {
                case 0:
                    _a = [];
                    for (_b in data)
                        _a.push(_b);
                    _i = 0;
                    _c.label = 1;
                case 1:
                    if (!(_i < _a.length)) return [3 /*break*/, 4];
                    i = _a[_i];
                    return [4 /*yield*/, data[i]];
                case 2:
                    _c.sent();
                    _c.label = 3;
                case 3:
                    _i++;
                    return [3 /*break*/, 1];
                case 4: return [2 /*return*/];
            }
        });
    }
    return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["from"])(iter());
}
function observableFrom(data) {
    function iter() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, data];
                case 1:
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    }
    return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["from"])(iter());
}



/***/ }),

/***/ "./src/app/service/redux/store.ts":
/*!****************************************!*\
  !*** ./src/app/service/redux/store.ts ***!
  \****************************************/
/*! exports provided: UpdateType, reduxStore */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UpdateType", function() { return UpdateType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reduxStore", function() { return reduxStore; });
/* harmony import */ var redux__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! redux */ "./node_modules/redux/es/redux.js");

var UpdateType;
(function (UpdateType) {
    UpdateType["add"] = "add";
    UpdateType["edit"] = "edit";
    UpdateType["delete"] = "delete";
})(UpdateType || (UpdateType = {}));
function reducer(_prevState, _action) {
    if (_prevState === void 0) { _prevState = {}; }
    return {};
}
var reduxStore = Object(redux__WEBPACK_IMPORTED_MODULE_0__["createStore"])(reducer);



/***/ }),

/***/ "./src/app/service/service.module.ts":
/*!*******************************************!*\
  !*** ./src/app/service/service.module.ts ***!
  \*******************************************/
/*! exports provided: ServiceModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceModule", function() { return ServiceModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _TreeData_io_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./TreeData.io.service */ "./src/app/service/TreeData.io.service.ts");




var ServiceModule = /** @class */ (function () {
    function ServiceModule() {
    }
    ServiceModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"]],
            providers: [_TreeData_io_service__WEBPACK_IMPORTED_MODULE_3__["TreeDataIoService"]]
        })
    ], ServiceModule);
    return ServiceModule;
}());



/***/ }),

/***/ "./src/app/type/CountryEnum.ts":
/*!*************************************!*\
  !*** ./src/app/type/CountryEnum.ts ***!
  \*************************************/
/*! exports provided: childType, CountryEnum */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "childType", function() { return childType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CountryEnum", function() { return CountryEnum; });
var CountryEnum;
(function (CountryEnum) {
    CountryEnum["Country"] = "Country";
    CountryEnum["State"] = "State";
    CountryEnum["Region"] = "Region";
    CountryEnum["City"] = "City";
})(CountryEnum || (CountryEnum = {}));
function childType(enum1) {
    var _a;
    var map = (_a = {},
        _a[CountryEnum.Country] = CountryEnum.State,
        _a[CountryEnum.State] = CountryEnum.Region,
        _a[CountryEnum.Region] = CountryEnum.City,
        _a);
    return map[enum1];
}



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_components_app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/components/app/app.module */ "./src/app/components/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_components_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /home/imo/Documents/dev/test/natek/angular-tree/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map